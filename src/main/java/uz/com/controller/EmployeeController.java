package uz.com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.com.model.Employee;
import uz.com.repository.EmployeeRepository;

import java.util.List;
@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class EmployeeController {

    @Autowired
    public EmployeeRepository employeeRepository;

    //get all Employee
    @GetMapping("employees")
    public List<Employee> getAllEmployee(){
        return employeeRepository.findAll();
    }
    //hello i am developer
}
